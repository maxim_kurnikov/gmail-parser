# -*- coding: utf-8 -*-
from __future__ import division, print_function, \
    unicode_literals, absolute_import

SCOPE = 'https://www.googleapis.com/auth/gmail.modify'

import os
PROJECT_ROOT = os.path.dirname(__file__)
CLIENT_SECRET_FILE = os.path.join(PROJECT_ROOT, 'client_secret.json')

SYNC_LABEL_NAME = 'tosync'

POLLING_INTERVAL = 1 # in minutes

# DB credentials
DB_HOST = 'localhost'
DB_USER = 'mkurnikov'
DB_PASSWORD = ''
DB_DATABASE = 'db'

