# -*- coding: utf-8 -*-
from __future__ import division, print_function, \
    unicode_literals, absolute_import
import os
import httplib2

import oauth2client.file
from oauth2client import client

import settings


def get_credentials():
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)

    credential_path = os.path.join(credential_dir,
                               'gmail-python-email-contacts.json')
    storage = oauth2client.file.Storage(credential_path)
    credentials = storage.get()

    if credentials is not None:
        if credentials.invalid:
            credentials.refresh(httplib2.Http())

    else:
        flow = client.flow_from_clientsecrets(filename=settings.CLIENT_SECRET_FILE,
                                              scope=settings.SCOPE,
                                              redirect_uri='http://localhost:8083')
        flow.params['access_type'] = 'offline'
        authorize_url = flow.step1_get_authorize_url()
        print(authorize_url)

        auth_code = raw_input('Auth code: ')
        credentials = flow.step2_exchange(code=auth_code)
        storage.put(credentials)

    return credentials







