# -*- coding: utf-8 -*-
from __future__ import division, print_function, \
    unicode_literals, absolute_import
import httplib2
import base64
from io import StringIO
from collections import OrderedDict
import simplejson as json
import time

from apiclient import discovery

from gmail_api.gmail_auth import get_credentials
import settings


def parse_labeled_threads_for_messages(label_id, gmail_service):
    threads_collection = gmail_service.users().threads().list(userId='me', labelIds=[label_id]).execute()

    messages = []
    if 'threads' in threads_collection:
        for thread_metadata in threads_collection['threads']:
            # Extract thread with /threads/threadId API call.
            thread_id = thread_metadata['id']
            thread = gmail_service.users().threads().get(userId='me', id=thread_id).execute()

            # Extract first message of thread with /messages/messageId API call.
            first_message_id = thread['messages'][0]['id']
            first_message = gmail_service.users().messages().get(userId='me', id=first_message_id,
                                                                       format='full').execute()

            # Extract text from message object. Message is multipart, each part is different representatipn of text.
            # We need 'text/plain' representation.
            message_representations = first_message['payload']['parts']
            for representation in message_representations:
                if representation['mimeType'] == 'text/plain':
                    message_body = representation['body']
                    data = message_body['data']
                    text = base64.urlsafe_b64decode(bytes(data)).decode('utf-8')
                    messages.append(text)

            # Remove 'sync_label_id' label from thread metadata with /threads/threadId/modify API call
            gmail_service.users().threads().modify(userId='me', id=thread_id, body={
                'removeLabelIds': [sync_label_id]
            }).execute()

    return messages


def parse_contact_information(text):
    text_stream = StringIO(text)

    contacts = OrderedDict()
    for line in text_stream.readlines():
        if not line.isspace():
            key, value = line.strip().split(':')
            contacts[key] = value.strip()

    return contacts


if __name__ == '__main__':
    credentials = get_credentials()
    # print('hi')
    http = credentials.authorize(httplib2.Http())
    gmail_service = discovery.build('gmail', 'v1', http=http)

    labels = gmail_service.users().labels().list(userId='me').execute()
    for label in labels['labels']:
        if label['name'] == settings.SYNC_LABEL_NAME:
            sync_label_id = label['id']
            break
    else:
        raise ValueError('{} not found in user labels.'.format(settings.SYNC_LABEL_NAME))
    #
    # import pymysql
    # connection = pymysql.connect(host=settings.DB_HOST,
    #                              user=settings.DB_USER,
    #                              password=settings.DB_PASSWORD,
    #                              db=settings.DB_DATABASE)

    while True:
        messages = parse_labeled_threads_for_messages(sync_label_id, gmail_service)

        contact_records = []
        for message in messages:
            contact_record = parse_contact_information(message)
            print(json.dumps(contact_record, indent=4, ensure_ascii=False))
        # break

        time.sleep(settings.POLLING_INTERVAL)



